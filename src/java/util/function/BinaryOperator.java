package java.util.function;

import java.util.Comparator;
import java.util.Objects;
/*
 *  BinaryOperator<Integer> adder = (n1,n2) -> n1 + n2;
 *  System.out.println(adder.apply(3, 4));
 * */

@FunctionalInterface
public interface BinaryOperator<T> extends BiFunction<T,T,T>{
    /*
     * 取自然排序最小的那个，这里默认升序
     * BinaryOperator<Integer> biN = BinaryOperator.minBy(Comparator.naturalOrder());
     * System.out.println(biN.apply(2, 3));
     * 结果 2
     *
     * 取反排序最小的那个，这里默认降序
     * BinaryOperator<Integer> biR = BinaryOperator.minBy(Comparator.reverseOrder());
     * System.out.println(biR.apply(2, 3));
     * 结果 3
     * */
    static <T> BinaryOperator<T> minBy(Comparator<? super T> comparator){
        Objects.requireNonNull(comparator);
        return (a,b)->comparator.compare(a,b)<=0?a:b;
    }

    /*
     * 取自然排序最大的那个，这里默认升序
     * BinaryOperator<Integer> biM = BinaryOperator.maxBy(Comparator.naturalOrder());
     * System.out.println(biM.apply(2, 3));
     * 结果 3
     *
     * 取反排序最大的那个，这里默认降序
     * BinaryOperator<Integer> biT = BinaryOperator.maxBy(Comparator.reverseOrder());
     * System.out.println(biT.apply(2, 3));
     * 结果 2
     * */
    static <T> BinaryOperator<T> maxBy(Comparator<? super T> comparator){
        Objects.requireNonNull(comparator);
        return (a,b)->comparator.compare(a,b)>=0?a:b;
    }
}
