package java.util.function;

@FunctionalInterface
public interface ToDoubleBiFunction<T,U>{
    /*
    * ToDoubleBiFunction<Integer,Long> i  = (x,y)-> Math.sin(x)+Math.sin(y);
    * System.out.println(i.applyAsDouble(Integer.MAX_VALUE, Long.MAX_VALUE));
    * 结果 0.27501382152888576
    * 2个参数处理结束 封装到 Double 并返回
    * */
    double applyAsDouble(T t,U u);
}
