package java.util.function;

@FunctionalInterface
public interface LongFunction<R>{
    /*
    * LongFunction<String> i = (l) -> Long.toString(l);
    * System.out.println(i.apply(Long.MAX_VALUE));
    * 结果 9223372036854775807
    * 作为功能函数过程，有参数和返回，处理结束，按照定义类型返回对应结果
    * */
    R apply(long value);
}
