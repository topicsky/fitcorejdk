package java.util.function;

@FunctionalInterface
public interface UnaryOperator<T> extends Function<T,T>{
    /*
    * UnaryOperator<String> i  = (x)-> x.toUpperCase();
    * System.out.println(i.apply("www.topicsky.top"));
    * 结果 WWW.TOPICSKY.TOP
    * 作为唯一参数函数接口，处理逻辑结束封装并返回 String
    */
    static <T> UnaryOperator<T> identity(){
        return t->t;
    }
}
