package java.util.function;

@FunctionalInterface
public interface DoubleSupplier{
    /*
    * DoubleSupplier pi = () -> {return 3.14d;};
    * System.out.println(pi.getAsDouble());
    * ��� 3.14
    */
    double getAsDouble();
}
