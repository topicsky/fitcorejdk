package java.util.function;

@FunctionalInterface
public interface IntSupplier{
    /*
    *  IntSupplier i = ()-> Integer.MAX_VALUE;
    *  System.out.println(i.getAsInt());
    *  结果 2147483647
    *  作为一个供应者，无偿提供一些什么
    *
    *  这里有个相对好理解的例子
    *  IntSupplier fib = new IntSupplier() {
    *       这里对这个接口进行了实现自定义
    *       这是前一个整数变量
    *       private int previous = 0;
    *       这是当前整数变量
    *       private int current = 1;
    *
    *       public int getAsInt() {
    *           这里对取数做了新规定，当然你也可以自定义
    *           int nextValue = this.previous + this.current;
    *           this.previous = this.current;
    *           this.current = nextValue;
    *           return this.previous;
    *       }
    *  };
    *  通过 整数流生产出一个无限流，将新的自定义的规则用于生产过程，最终限制只要前10个结果，遍历并输出
    *  IntStream.generate(fib).limit(10).forEach(System.out::println);
    *
    * 结果
    *       1
    *       1
    *       2
    *       3
    *       5
    *       8
    *       13
    *       21
    *       34
    *       55
    * */
    int getAsInt();
}
