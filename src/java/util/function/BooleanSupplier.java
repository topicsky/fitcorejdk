package java.util.function;

/*
 * 作为供应者提供返回，无参
 * BooleanSupplier bs = () -> true;
 * System.out.println(bs.getAsBoolean());
 * 结果 true
 * <p>
 * int x = 0, y= 1;
 * 同样可以做一些计算比较
 * bs = () -> x > y;
 * System.out.println(bs.getAsBoolean());
 * 结果 false
 */
@FunctionalInterface
public interface BooleanSupplier{
    /*
     * BooleanSupplier bs = () -> true;
     * System.out.println(bs.getAsBoolean());
     * 结果 true
     */
    boolean getAsBoolean();
}
