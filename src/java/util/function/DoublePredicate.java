package java.util.function;

import java.util.Objects;

/*
* DoublePredicate dp = (d) -> d > 0;
* System.out.println(dp.test(0.5));
* 结果 true
* */
@FunctionalInterface
public interface DoublePredicate{
    /*
    *  DoublePredicate dp = (d) -> d > 0;
    *  DoublePredicate dp1 = (d) -> d == 0;
    *  System.out.println(dp.and(dp1).test(0.5));
    *  先 dp.test(0.5)
    *  结果 true
    *  然后 dp1.test(0.5)
    *  结果 false
    *  最后结果 false
    * */
    default DoublePredicate and(DoublePredicate other){
        Objects.requireNonNull(other);
        return (value)->test(value)&&other.test(value);
    }
    /*
    * DoublePredicate dp = (d) -> d > 0;
    * System.out.println(dp.test(0.5));
    * 结果 true
    * */
    boolean test(double value);

    /*
    *  DoublePredicate dp = (d) -> d > 0;
    *  System.out.println(dp.test(0.5));
    *  结果 true
    *  System.out.println(dp.negate().test(0.5));
    *  结果取反
    *  结果 flase
    */
    default DoublePredicate negate(){
        return (value)->!test(value);
    }

    /*
    *
    *  DoublePredicate dp = (d) -> d > 0;
    *  DoublePredicate dp1 = (d) -> d == 0;
    *  System.out.println(dp.or(dp1).test(0.5));
    *  先 dp.test(0.5)
    *  结果 true
    *  然后 dp1.test(0.5)
    *  结果 false
    *  最后结果 true
    */
    default DoublePredicate or(DoublePredicate other){
        /*
        * 例行检查
        * */
        Objects.requireNonNull(other);
        return (value)->test(value)||other.test(value);
    }
}
