package java.util.function;

@FunctionalInterface
public interface DoubleToIntFunction{
    /*
    *  DoubleToIntFunction df = (x) -> {return (int)x+2;};
    *  System.out.println(df.applyAsInt(3.14));
    *  ��� 5
    */
    int applyAsInt(double value);
}
