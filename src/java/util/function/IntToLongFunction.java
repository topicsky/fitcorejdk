package java.util.function;

@FunctionalInterface
public interface IntToLongFunction{
    /*
    * IntToLongFunction i = (x) -> Long.MAX_VALUE-x;
    * System.out.println(i.applyAsLong(2));
    * 结果（参数做完运算封装到 Long 并返回）
    * 9223372036854775805
    * */
    long applyAsLong(int value);
}
