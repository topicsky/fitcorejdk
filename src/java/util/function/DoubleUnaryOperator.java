package java.util.function;

import java.util.Objects;

/*
* DoubleUnaryOperator dl = (x) -> {return x*x;};
* System.out.println(dl.applyAsDouble(3.14));
* 结果 9.8596
* */
@FunctionalInterface
public interface DoubleUnaryOperator{
    /*
    * DoubleUnaryOperator id = DoubleUnaryOperator.identity();
    * System.out.println(id.applyAsDouble(3.14));
    * 结果 3.14
    * 这是恒等式，将之转化为同等值得不同类型
    */
    static DoubleUnaryOperator identity(){
        return t->t;
    }

    /*
    *  DoubleUnaryOperator square = (x) -> {return x*x;};
    *  DoubleUnaryOperator doubleValue = (x) -> {return 2*x;};
    *  System.out.println(doubleValue.compose(square).applyAsDouble(3.14));
    *  先 square.applyAsDouble(3.14)
    *  结果 9.8596
    *  在 doubleValue.applyAsDouble(9.8596)，注意 compose 的参数和调用方顺序
    *  结果 9.8596 * 2 = 19.7192
    *  最终结果 19.7192
    */
    default DoubleUnaryOperator compose(DoubleUnaryOperator before){
        Objects.requireNonNull(before);
        return (double v)->applyAsDouble(before.applyAsDouble(v));
    }
    /*
    * DoubleUnaryOperator dl = (x) -> {return x*x;};
    * System.out.println(dl.applyAsDouble(3.14));
    * 结果 9.8596
    * */
    double applyAsDouble(double operand);

    /*
    *
    *  DoubleUnaryOperator square = (x) -> {return x*x;};
    *  DoubleUnaryOperator doubleValue = (x) -> {return 2*x;};
    *  System.out.println(doubleValue.andThen(square).applyAsDouble(3.14));
    *  不同于 compose ，顺序不一样
    *  先 doubleValue..applyAsDouble(3.14)
    *  结果 6.28
    *  然后 square.applyAsDouble(6.28)，注意 andThen 的参数和调用方顺序
    *  结果 6.28* 6.28 = 39.4384
    *  最终结果 39.4384
    */
    default DoubleUnaryOperator andThen(DoubleUnaryOperator after){
        Objects.requireNonNull(after);
        return (double t)->after.applyAsDouble(applyAsDouble(t));
    }
}
